﻿namespace AudioManager{

/// <summary>
/// BGMファイルへのパスを定数で管理するクラス
/// </summary>
public static class BGMPath{

	public const string BGM_BLOCK        = "BGM/bgm_block";
	public const string FULL             = "BGM/full";
	public const string NYAN100_CARNIVAL = "BGM/Nyan100_Carnival";

}

}
