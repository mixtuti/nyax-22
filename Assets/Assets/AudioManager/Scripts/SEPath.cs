﻿namespace AudioManager{

/// <summary>
/// SEファイルへのパスを定数で管理するクラス
/// </summary>
public static class SEPath{

	public const string CAT_ROBOT   = "SE/CatRobot";
	public const string HIT         = "SE/hit";
	public const string HIT_ANOTHER = "SE/hit_another";
	public const string HIT_RACKET  = "SE/hit_racket";
	public const string WALL_HIT    = "SE/wall_hit";

}

}
